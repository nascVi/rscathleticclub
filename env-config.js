const prod = process.env.NODE_ENV === 'production';

module.exports = {
  'process.env.BASE_URL': prod ? 'https://rscathleticclub.herokuapp.com' : 'http://localhost:3000',
  'process.env.NAMESPACE': 'https://rscathleticclub.herokuapp.com',
  'process.env.CLIENT_ID': '0bgv21rYA00hYDU8QsSas41rnivyCgv0'
}
